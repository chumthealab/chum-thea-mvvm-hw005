//
//  ArticleService.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ArticleService {
    
    func fetchArticles(page: Int, complitionHandler: @escaping ([Article]) -> ()){
        
        AF.request(ConfigConstant.FETCH_URL + "?page=\(page)&limit=15").response { (response) in
            if let data = response.data {
                print(data)
                do {
                    let dataProviding = try JSONDecoder().decode(DataProviding.self, from: data)
                    
                    complitionHandler(dataProviding.data)
                    
                } catch {
                    print("Error1: \(error.localizedDescription)")
                    complitionHandler([])
                }
                
            }else {
                print("Error: \(response.error?.localizedDescription)")
                complitionHandler([])
            }
        }
    }
    
    
    func addArticle(article: ArticleModel, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()) {
        
        let params : [String:Any] = [
            "TITLE": article.title!,
        "DESCRIPTION": article.description!,
        "AUTHOR": article.author?.id,
        "CATEGORY_ID": article.category?.id,
        "STATUS": "1",
        "IMAGE": article.thumbnail!
        ]
        
        AF.request(ConfigConstant.ADD_URL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: ConfigConstant.HEADERS, interceptor: nil).response { (response) in
            switch response.result {
                
            case .success(let data) :

                let json = try? JSON(data: data!)
                let message = json!["MESSAGE"].stringValue
                let status = json!["DATA"]["STATUS"].boolValue
                
                print("status: \(status)")
                complitionHandler(message, status)
                
            case .failure(let error) :
                print(error)
                
            }
        }
        
        
    }
    
    
    func delete(id: Int, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()){
        AF.request(ConfigConstant.FETCH_URL + "/\(id)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: ConfigConstant.HEADERS, interceptor: nil).response { (response) in
            switch response.result {
            case .success(let data):
                print(data)
                let json = try? JSON(data: response.data!)
                print(response)
                let message = json!["MESSAGE"].stringValue
                let status = json!["DATA"]["STATUS"].boolValue
                
                complitionHandler(message, status)
            case .failure(let error):
                print("ERROR: \(error)")
                
            }
        }
    }
    
    
    func update(article: ArticleModel, complitionHandler: @escaping (_ message: String, _ bool: Bool) -> ()){
        let params : [String:Any] = [
            "ID": article.id!,
            "TITLE": article.title!,
        "DESCRIPTION": article.description!,
        "AUTHOR": article.author?.id,
        "CATEGORY_ID": article.category?.id,
        "STATUS": "1",
        "IMAGE": article.thumbnail!
        ]
        
        AF.request(ConfigConstant.FETCH_URL + "/\(article.id!)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: ConfigConstant.HEADERS, interceptor: nil).response { (response) in
            switch response.result {
                
            case .success(let data) :

                let json = try? JSON(data: data!)
                let message = json!["MESSAGE"].stringValue
                let status = json!["DATA"]["STATUS"].boolValue
                
                print("status: \(status)")
                complitionHandler(message, status)
                
            case .failure(let error) :
                print(error)
                
            }
        }

    }
    
    
    func uploadImage(data: Data, article: ArticleModel, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()){
        AF.upload(multipartFormData: { (formData) in
            formData.append(data, withName: "FILE", fileName: "image.jpg", mimeType: "image/*")
        }, to: ConfigConstant.UPLOAD_SINGLE_FILE, method: .post, headers: ConfigConstant.HEADERS).response { (response) in
            
            switch response.result {
            case .success(let data):
                let json = try? JSON(data: data!)
                let imgURL = json!["DATA"].stringValue
                
                article.thumbnail = imgURL
                
                if article.id == 0 {
                    self.addArticle(article: article) { (message, status) in
                        complitionHandler(message, status)
                    }
                }else {
                    self.update(article: article) { (message, status) in
                        complitionHandler(message, status)
                    }
                }
                
            case .failure(let error):
                print(error)
            }
        }
        
        
        
    }
}


