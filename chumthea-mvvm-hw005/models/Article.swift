//
//  Article.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

struct Article : Codable {
    let id: Int
    let title, description, createdDate: String
    let author: Author
    let status: String
    let category: Category
    let image: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case title = "TITLE"
        case description = "DESCRIPTION"
        case createdDate = "CREATED_DATE"
        case author = "AUTHOR"
        case status = "STATUS"
        case category = "CATEGORY"
        case image = "IMAGE"
    }
}
