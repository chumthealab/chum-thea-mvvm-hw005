//
//  Author.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - Author
struct Author: Codable {
    var id: Int
    var name, email, gender, telephone: String?
    var status, facebookID, imageURL: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "NAME"
        case email = "EMAIL"
        case gender = "GENDER"
        case telephone = "TELEPHONE"
        case status = "STATUS"
        case facebookID = "FACEBOOK_ID"
        case imageURL = "IMAGE_URL"
    }

    init(id: Int){self.id = id}
}
