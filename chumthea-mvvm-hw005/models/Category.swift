//
//  Category.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - Category
struct Category: Codable {
    var id: Int
    var name: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case name = "NAME"
    }
    
    init(id: Int) {
        self.id = id
    }
}
