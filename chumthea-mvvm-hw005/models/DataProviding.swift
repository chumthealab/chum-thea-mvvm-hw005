//
//  DataProviding.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - DataProviding
struct DataProviding: Codable {
    let code, message: String
    let data: [Article]
    let pagination: Pagination

    enum CodingKeys: String, CodingKey {
        case code = "CODE"
        case message = "MESSAGE"
        case data = "DATA"
        case pagination = "PAGINATION"
    }
}
