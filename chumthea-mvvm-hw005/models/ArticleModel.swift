//
//  ArticleModel.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - ArticleModel

class ArticleModel {
    
    var id: Int?
    var title: String?
    var description: String?
    var createdDate: String?
    var author: Author?
    var category: Category?
    var thumbnail: String?
    
    init(article: Article){
        self.id = article.id
        self.title = article.title
        self.description = article.description
        self.createdDate = article.createdDate
        self.author = article.author
        self.category = article.category
        self.thumbnail = article.image
    }
    
    init(id: Int, title: String?, description: String?, author: Author?, category: Category?, thumbnail: String?) {
        self.id = id
        self.title = title
        self.description = description
        self.author = author
        self.category = category
        self.thumbnail = thumbnail
    }
    
    init() {}
    
}
