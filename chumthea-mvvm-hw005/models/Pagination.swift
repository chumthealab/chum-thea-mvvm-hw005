//
//  Pagination.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - Pagination
struct Pagination: Codable {
    var page, limit, totalCount, totalPages: Int

    enum CodingKeys: String, CodingKey {
        case page = "PAGE"
        case limit = "LIMIT"
        case totalCount = "TOTAL_COUNT"
        case totalPages = "TOTAL_PAGES"
    }
}
