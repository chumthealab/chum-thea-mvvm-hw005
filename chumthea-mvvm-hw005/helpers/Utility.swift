//
//  Utility.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class Utility {
    
    static func styleRoundButton(_ button: UIButton) {
        button.backgroundColor = .red
        button.layer.cornerRadius = 25
        
    }
    
    static func styleNavigationBar() {
        
       let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithOpaqueBackground()
        coloredAppearance.backgroundColor = .systemRed
        coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
               
        UINavigationBar.appearance().standardAppearance = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
    }
    
    static func styleImageView(_ imageView: UIImageView) {
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }
    
    static func styleFloatButton(_ button: UIButton, imageName: String) {
        

        button.layer.cornerRadius = button.frame.width / 2
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 22, green: 0, blue: 0, alpha: 1)
        button.setImage(UIImage(systemName: imageName), for: .normal)
        button.tintColor = .white
    }
    
    
    
    
    static var views: [Int] = [
        10000,
        20000,
        10,
        2000,
        3099,
        4522,
        90022,
        980,
        800,
        300009498
    ]
    
    
    
    static var authors: [String] = [
        "Sok San",
        "Sok Sovan",
        "Mina",
        "Koko Si",
        "Kimsay Sii",
        "Dara Kok",
        "Siekny Sii",
        "Kimleang Kea",
        "Sinh Ley",
        "Kimi no konoto"
    ]

    
}

extension UITextField {
    
    func underlined(){
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height + 5, width: 0.95 * self.frame.width, height: 2.0)
        
        bottomLine.backgroundColor = UIColor.red.cgColor
        
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
    }
    
}

extension String {
    
    func customDateFormatter() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyyMMddHHmmss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, yyyy"

        if let date = dateFormatterGet.date(from: self) {
            return dateFormatterPrint.string(from: date)
        } else {
           print("There was an error decoding the string")
            return ""
        }
    }
    
}

// MARK: - Custom Abbreviation Number for like and comments
extension Int{
    
    func abbreviationNum() -> String{
        if self < 1000 {
            return "\(self)"
        }else if self < 1000000 {
            var n = Double(self)
            n = Double(floor(n/100)/10)
            return "\(n.description)K"
        }else {
            var n = Double(self)
            n = Double(floor(n/10000) / 100)
            return "\(n.description)M"
        }
    }
    
}

extension UIButton {
    
    func slideButton(){
        UIView.animate(withDuration: 0.5) {
            let frame = self.frame
            self.frame = CGRect(x: frame.origin.x, y: frame.origin.y - 0.2 * frame.origin.y, width: frame.width, height: frame.height)
        }
    }

}

