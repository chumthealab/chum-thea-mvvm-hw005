//
//  ConfigConstant.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

import Foundation
import Alamofire

class ConfigConstant {
    
    private static let MAIN_URL = "http://110.74.194.124:15011"
    
    static let FETCH_URL = MAIN_URL + "/v1/api/articles"
    
    static let ADD_URL = MAIN_URL + "/v1/api/articles"
    
    static let UPLOAD_SINGLE_FILE = MAIN_URL + "/v1/api/uploadfile/single"
    
    private static let HEADERS_DICT : [String:String] = [
        "Content-Type": "application/json",
        "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
    ]
    
    static let HEADERS = HTTPHeaders(HEADERS_DICT)
}
