//
//  ViewConstant.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

import Foundation

class ViewConstant {
    
    struct StoryBoard {
        static let homeVC = "HomeViewController"
        static let addVC = "AddViewController"
    }
    
    static let REUSABLE_CELL = "CellReusableIdentifier"
}
