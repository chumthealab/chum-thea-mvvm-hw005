//
//  ArticleViewModel.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

// MARK: - ArticleViewModel

class ArticleViewModel {
    
    
    var articleService = ArticleService()
    
    func fetchArticles(page: Int, complitionHandler: @escaping ([ArticleModel]) -> ()){
        articleService.fetchArticles(page: page) { (articles) in
            
            complitionHandler(articles.flatMap(ArticleModel.init))
            
        }
    }
    
    
    func addArticle(article: ArticleModel, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()) {
        
        articleService.addArticle(article: article) { (message, status) in
            complitionHandler(message, status)
        }
        
    }
    
    func deleteArticle(id: Int, complitionHandler: @escaping (_ message: String, _ status: Bool) -> (Void)){
        articleService.delete(id: id) { (message, status) in
            complitionHandler(message, status)
        }
    }
    
    func updateArticle(article: ArticleModel, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()){
        articleService.update(article: article) { (message, status) in
            complitionHandler(message, status)
        }
    }
    
    func uploadImage(data: Data, article: ArticleModel, complitionHandler: @escaping (_ message: String, _ status: Bool) -> ()) {
        articleService.uploadImage(data: data, article: article) { (message, status) in
            complitionHandler(message, status)
        }
    }
}
