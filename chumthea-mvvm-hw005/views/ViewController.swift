//
//  ViewController.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var page = 1
    var sweetAlert = SweetAlert()
    
    
    let articleViewModel = ArticleViewModel()
    var articles = [ArticleModel]()
    
    var refreshControl: UIRefreshControl?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        
        articleViewModel.fetchArticles(page: page) { (articleModels) in
                   self.articles = articleModels
                   self.tableView.reloadData()
               }
                       
               tableView.register(UINib(nibName: "CustomTableViewCell", bundle: nil), forCellReuseIdentifier: ViewConstant.REUSABLE_CELL)
               
               
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(refreshTableViewController(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshTableViewController(_ refreshControll: UIRefreshControl){
        page = 1
        articleViewModel.fetchArticles(page: page) { (articleModels) in
            self.articles = articleModels
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        page = 1
        articleViewModel.fetchArticles(page: page) { (articleModels) in
            self.articles = articleModels
            self.tableView.reloadData()
        }
    }



}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ViewConstant.REUSABLE_CELL, for: indexPath) as! CustomTableViewCell
        
        cell.setUp(article: articles[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      
        if (indexPath.row == self.articles.count - 1) {
            page = page + 1
            self.articleViewModel.fetchArticles(page: page) { (articleModels) in
                self.articles += articleModels
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (_, _, _) in
            
            _ = self.sweetAlert.showAlert("Warning", subTitle: "Do you want to delete it?", style: .warning, buttonTitle: "Delete", buttonColor: .green, otherButtonTitle: "Cancel") { (isOtherButton) in
                if isOtherButton {
                    self.articleViewModel.deleteArticle(id: self.articles[indexPath.row].id!) { (message, status) -> (Void) in
                        if status {
                            self.articles.remove(at: indexPath.row)
                            self.tableView.deleteRows(at: [indexPath], with: .fade)
                            self.tableView.reloadData()
                        }else {
                            print("Cannot delete")
                        }
                    }
                }
            }
            
            
            
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title: "Edit") { (_, _, _) in
            let editViewController = self.storyboard?.instantiateViewController(identifier: "addVC") as! AddViewController
            
            editViewController.article = self.articles[indexPath.row]
            editViewController.imageName = "pencil"
            editViewController.title = "Update"
            
            self.navigationController?.pushViewController(editViewController, animated: true)
        }
        
        return UISwipeActionsConfiguration(actions: [edit])
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = self.storyboard?.instantiateViewController(identifier: "DetailsVC") as! DetailsViewController
        
        detailVC.article = articles[indexPath.row]
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
}

