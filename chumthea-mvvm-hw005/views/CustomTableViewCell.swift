//
//  CustomTableViewCell.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
   override func awakeFromNib() {
        super.awakeFromNib()
        Utility.styleImageView(thumbnailImageView)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // prepare to set article to tableview cell
    
    func setUp(article: ArticleModel) {
        
        titleLabel.text = "\(article.title! == "" ? "No title" : article.title ?? "No title")"
        
        let views = Utility.views[Int.random(in: 0 ..< 10)]
        
        viewLabel.text = "View: \(views.abbreviationNum()) \(views > 1 ? "Views":"View")"
        dateLabel.text = "Date: \(article.createdDate!.customDateFormatter())"
        authorLabel.text = "Author: \(Utility.authors[Int.random(in: 0 ..< 10)])"
        
        let url = URL(string: article.thumbnail ?? "")
        thumbnailImageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [
               .scaleFactor(UIScreen.main.scale),
               .transition(.fade(1)),
               .cacheOriginalImage
        ])
    }
    
}
