//
//  AddViewController.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import Kingfisher

class AddViewController: UIViewController {

    var sweetAlert = SweetAlert()
    var articleViewModel = ArticleViewModel()
    var article: ArticleModel?
    
    var imagePickerController: UIImagePickerController!
    
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    
    var imageName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.slideButton()
        
        if  imageName == nil {
            imageName = "plus"
        }
        
        
        Utility.styleImageView(imageView)
        titleTextField.underlined()
        Utility.styleFloatButton(addButton, imageName: imageName!)
        
        if let article = article {
            titleTextField.text = "\(article.title == "" ? "No Title" : article.title!)"
            descriptionTextField.text = "\(article.description == "" ? "No Description" : article.description!)"
            imageView.kf.setImage(with: URL(string: article.thumbnail ?? ""), placeholder: UIImage(named: "placeholder"), options: [
            .cacheOriginalImage
            ])
        }
        
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        
        let tapGuester = UITapGestureRecognizer(target: self, action: #selector(pickImageFromLibrary(_:)))
        imageView.addGestureRecognizer(tapGuester)
        
        imageView.isUserInteractionEnabled = true
        
    }
    
    @objc func pickImageFromLibrary(_ guester: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Photo", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (action) in
            self.imagePickerController.sourceType = .camera
            self.present(self.imagePickerController, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action) in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        
        if let article = article {
            let articleToUpdate = ArticleModel(id: article.id!,
                                               title: titleTextField.text!,
                                               description: descriptionTextField.text,
                                               author: Author(id: 1),
                                               category: Category(id: 1),
                                               thumbnail: "")
            
            let imageData = imageView.image?.pngData()
            
            articleViewModel.uploadImage(data: imageData!, article: articleToUpdate) { (message, status) in

                DispatchQueue.main.async {
                   if status {
                    _ = self.sweetAlert.showAlert("Success!!!", subTitle: message, style: .success)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            
            
        }else {
            
            let title = titleTextField.text
            let description = descriptionTextField.text
            
            let author = Author(id: 1)
            let category = Category(id: 1)
            
            let myArticleModel = ArticleModel(id: 0,
                                              title: title!,
                                              description: description!,
                                              author: author,
                                              category: category,
                                              thumbnail: "")
            
            let imgData = imageView.image?.pngData()
            
            
            articleViewModel.uploadImage(data: imgData!, article: myArticleModel) { (message, status) in
                DispatchQueue.main.async {
                   if status {
                    _ = self.sweetAlert.showAlert("Success!!!", subTitle: message, style: .success)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }

        }
    }

}

extension AddViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            imageView.image = image
        }
        imagePickerController.dismiss(animated: true, completion: nil)
    }
}
