//
//  DetailsViewController.swift
//  chumthea-mvvm-hw005
//
//  Created by MacBook on 12/26/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    var article: ArticleModel?
    var sweetAlert = SweetAlert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        Utility.styleImageView(imageView)
        
        // prepare to set article on screen
        
        if let article = article {
            titleLabel.text = "\(article.title! == "" ? "No title" : article.title ?? "No title")"
            let views = Utility.views[Int.random(in: 0 ..< 10)]
            dateLabel.text = "Date: \(article.createdDate!.customDateFormatter()) | \(views.abbreviationNum()) \(views > 1 ? "Views" : "View")"
            
            typeLabel.text = "Type: \(article.category?.name ?? "No Type")"
            descriptionTextView.text = "\(article.description == "" ? "No Any Description" : article.description!)"
            
           
            
           let url = URL(string: article.thumbnail ?? "")
            imageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [
                   .scaleFactor(UIScreen.main.scale),
                   .transition(.fade(1)),
                   .cacheOriginalImage
            ])
        }
        
        // long pressed tap to enable download
        
        imageView.isUserInteractionEnabled = true
        let longPressed = UILongPressGestureRecognizer(target: self, action: #selector(saveImage(_:)))
        longPressed.minimumPressDuration = 0.5
        imageView.addGestureRecognizer(longPressed)

    }
    
     // MARK: - Save Image to photo
    
    @objc func saveImage(_ longPressed: UILongPressGestureRecognizer){

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save Photo", style: .default, handler: { (action) in
            print("Save")
            UIImageWriteToSavedPhotosAlbum(self.imageView.image!, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
            self.sweetAlert.showAlert("Save!", subTitle: "The image has been saved into your photo", style: .success)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            print("Cancel")
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @objc func image(image: UIImage!, didFinishSavingWithError error: NSError!, contextInfo: AnyObject!) {
        if (error != nil) {
            // Something wrong happened.
            print(error.localizedDescription)
        } else {
            // Everything is alright.
        }
    }

}
